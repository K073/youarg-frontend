import React, {Component} from 'react';


const wellStyles = { maxWidth: 400, margin: '0 auto 10px' };

class Login extends Component{
  render() {
    return (
      <div className="well" style={wellStyles}>
        <a href='http://lebedev.ddns.net:8000/vk/login' className="btn btn-block">
          Sign in with VK
        </a>
        <a href='http://lebedev.ddns.net:8000/facebook/login' className="btn btn-block">
          Sign in with Facebook
        </a>
      </div>
    );
  }
}

export default Login;