import React from 'react';
import {Badge, MenuItem, NavDropdown, NavItem} from "react-bootstrap";
import Wraper from "../../../hoc/Wraper";
import {LinkContainer} from "react-router-bootstrap";

class HeaderProfile extends React.Component {


  render() {
    return (
      <Wraper>
        {!this.props.userLogin ? <LinkContainer to={'/login'}><NavItem>Login</NavItem></LinkContainer> :
          <Wraper>
            <NavDropdown title={this.props.userData.first_name + ' ' + this.props.userData.last_name}>
              <MenuItem>Profile</MenuItem>
              <MenuItem divider/>
              <MenuItem onClick={this.props.logout}>Logout</MenuItem>
            </NavDropdown>
            <NavItem>Karma <Badge>{this.props.userData.karma_cached}</Badge></NavItem>
          </Wraper>
        }
      </Wraper>
    );
  }

}

export default HeaderProfile;