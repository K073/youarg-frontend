import React, {Component} from 'react'
import {FormControl, Nav, Navbar, NavItem} from "react-bootstrap";
import {Link} from "react-router-dom";
import HeaderProfile from "./HeaderProfile/HeaderProfile";
import {LinkContainer} from "react-router-bootstrap";

class Header extends Component{
  state = {
    searchInput: '',
    userLogin: false
  };

  searchHandler = e => {
    this.setState({searchInput: e.target.value})
  };

  render() {
    return (
      <Navbar collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>YouArg</Link>
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/tags'}>
              <NavItem>
                Tags
              </NavItem>
            </LinkContainer>
          </Nav>
          <Navbar.Form pullLeft>
            <FormControl
              type='text'
              value={this.state.searchInput}
              placeholder='Enter Text'
              onChange={this.searchHandler}
            />
          </Navbar.Form>
          <Nav pullRight>
            <HeaderProfile logout={this.props.logout} userLogin={this.props.userLogin} userData={this.props.userData} />
            <LinkContainer to={'/help'}>
              <NavItem>
                Help
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default Header